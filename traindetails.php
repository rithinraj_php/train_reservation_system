<?php 
session_start();
include 'database.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  
}

* {
  box-sizing: border-box;
}
.bg-image {
  /* The image used */
  background-image: url("bg.JPG");
  
  /* Add the blur effect */
  filter: (8px);
  -webkit-filter: (8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 50%;
  padding: 50px;
  text-align: center;
}
</style>

<title>Train reservation</title>

</head>
	
<body>
<div class="bg-image"></div>
<div class="bg-text">

<div>
  <?php
 
  if(!empty($_SESSION['SID']))
  {
    echo '<div id=""> ';
    echo '<tr>';
    echo "<td><span style='color:green;'>Welcome:</span> ".$_SESSION['SID']."</td>";

    echo '</tr>';
    echo '</div>';
  }
  else
  {
    echo '<div id=" "> ';
    echo '</div>';
  }
  ?>  
 </div>


<div>
            <?php 
				if($_GET){
					if($_GET['msg']=="Success")
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:green;">'.$_GET['msg'].'</label>';
					}
					else
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:red;">'.$_GET['msg'].'</label>';
			
					}	
				}				   
			?>
</div>
				
<table align="center">    
  <tr> 
    <td align="center">   
        <div> 
          	<form method="POST" action="traindetailscheck.php" enctype="multipart/form-data">
          		<h2><marquee>RAILWAY RESERVATION SYSTEM</marquee></h2>
          		<h1>Train details</h1>
              <table align="center">
                <tr>
                  <td align="left">PNR no: </td>
                  <td><input type="text" placeholder="PNR number"  name="pnrno" required></td>
                </tr>

                <tr>
                  <td align="left">Starting Place: </td>
                  <td><input type="text" placeholder="Starting place" name="start" required></td>
                </tr>

                <tr>
                  <td align="left">Ending Place: </td>
                  <td><input type="text" placeholder="Ending place" name="ending" required></td>
                </tr>

                <tr>
                  <td align="left">Date Of Journey: </td>
                  <td><input style="width: 69%" type="date" placeholder="Date of journey" name="date1" required></td>
                </tr>

                <tr>
                  <td align="left">TrainName: </td>
                  <td><select id="tname" name="trainname" style="width: 69%">
                      <option value=""> -Select Train- </option>
                      <option value="Karnataka Sampark Kranti Express">Karnataka Sampark Kranti Express</option>
                      <option value="Karnataka Express">Karnataka Express</option>
                      <option value="Karnavati Express">Karnavati Express</option>
                      <option value="Kanyakumari Express">Kanyakumari Express</option>
                      <option value="Delhi Express">Delhi Express</option>
                      <option value="Coimbatore Express">Coimbatore Express</option>
                    </select></td>
                </tr>

                <tr>
                  <td align="left">Ticket Price: </td>
                  <td><input type="text" id="tprice" style="width: 69%" placeholder="Ticket price" name="ticketprice" readonly style="background-color: white;border: 1px solid grey"></td>
                </tr>

                <tr>
                  <td align="left">Number of Tickets: </td>
                  <td><input id="tickets" onkeyup="ticket();" style="width: 69%" value="0" type="number" placeholder="Number of tickets"  name="notickets" min="1" max="5" required></td>
                </tr>

                <tr>
                  <td align="left">Total Price: </td>
                  <td><input id="total" readonly type="text" placeholder="total price" name="totalprice" required></td>
                </tr>

                <tr>
                  <td style="padding-top: 20px" align="right"><input disabled="true" id="submit" name="submit" type="submit" value="submit"></td>
                  <td style="padding-top: 20px" align="left"><a href="customer.php"><input type="button" value="Next"/></a></td>
                </tr>
              </table>    
          	</form>					
        </div>
    </td>
    
    <td align="right">
     <h3 >1. Search</h3>
     <h3 style="color: red;">2. Train details</h3>
     <h3>3. Passenger details</h3>
     <h3>4. Payment</h3>
     <h3>5. Print ticket</h3>
    </td>  
      
  </tr>
</table>



<script>
var e = document.getElementById("tname");
e.addEventListener("change", function() {
  var val = e.options[e.selectedIndex].value;
  console.log(val,'value')

if(val == 'Karnataka Sampark Kranti Express'){
  document.getElementById("tprice").value = 100 +'/-';
}

if(val == 'Karnataka Express'){
  document.getElementById("tprice").value = 200 + '/-';
}

if(val == 'Karnavati Express'){
  document.getElementById("tprice").value = 300 + '/-';
}

if(val == 'Kanyakumari Express'){
  document.getElementById("tprice").value = 400 + '/-';
}

if(val == 'Delhi Express'){
  document.getElementById("tprice").value = 500 + '/-';
}

if(val == 'Coimbatore Express'){
  document.getElementById("tprice").value = 600 + '/-';
}
if(val == ''){
document.getElementById("tprice").value = 0;	
}


});


// To show preselected value
// var val = e.options[e.selectedIndex].value;
// document.getElementById("tprice").value = val;


function ticket(){
var ticket = parseInt(document.getElementById('tickets').value);
// console.log(ticket,'ticket');
var ticketprice = parseInt(document.getElementById('tprice').value);
// console.log(ticketprice,'ticketprice')
var totalprice = ticket * ticketprice;
// console.log(totalprice,'totalprice');
document.getElementById('total').value = totalprice;

if(total <100){
document.getElementById('submit').disabled = true;
}
else{
 document.getElementById('submit').disabled = false; 
}

}




</script>

</body>
</html>