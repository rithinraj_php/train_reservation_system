-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2018 at 06:02 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `train`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm`
--

CREATE TABLE `adm` (
  `id` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adm`
--

INSERT INTO `adm` (`id`, `name`, `email`, `pass`) VALUES
(1, 'rithanya', 'r@gmail.com', '123'),
(2, 'seema', 's@gmail.com', '345'),
(3, 'tom', 'tom@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `care`
--

CREATE TABLE `care` (
  `id` int(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `complaint` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `care`
--

INSERT INTO `care` (`id`, `name`, `email`, `complaint`) VALUES
(1, 'rr', 'q@gmail.com', ''),
(2, 'yuuu', 'cccccc@gmail.com', 'ksdhkjsddscs,dk ncksjhs  djgksdgcfk dksduhfckusd'),
(3, 'abi', 'abi@gmail.com', 'train was late for an hour.'),
(4, 'vik', 'vi@gmail.com', 'train was late.'),
(5, 'lakshmi', 'laxmi271997@gmail.co', 'quality of service of is not good'),
(6, 'rrr', 't@gmail.com', 'train was late.');

-- --------------------------------------------------------

--
-- Table structure for table `journey`
--

CREATE TABLE `journey` (
  `id` int(5) NOT NULL,
  `journey_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journey`
--

INSERT INTO `journey` (`id`, `journey_date`) VALUES
(100, '2018-11-22'),
(101, '2018-11-22'),
(102, '2018-11-25'),
(103, '2018-11-10'),
(104, '2018-11-07'),
(105, '2018-11-05'),
(106, '2018-11-06'),
(107, '2018-11-02'),
(108, '2018-11-23'),
(109, '2018-11-29'),
(110, '2018-11-26'),
(111, '2018-11-23'),
(112, '2018-11-24'),
(113, '2018-11-24'),
(114, '2018-11-28'),
(115, '2018-11-19'),
(116, '2018-11-19'),
(117, '0000-00-00'),
(118, '2018-11-28'),
(119, '2018-11-28'),
(120, '2018-11-28'),
(121, '2018-11-28'),
(122, '2018-11-28'),
(123, '2018-11-28'),
(124, '2018-11-28'),
(125, '2018-11-18'),
(126, '2018-11-18'),
(127, '2018-11-18'),
(128, '2018-11-18'),
(129, '2018-11-18'),
(130, '2018-11-18'),
(131, '2018-11-23'),
(132, '2018-11-23'),
(133, '2018-11-23'),
(134, '2018-11-12'),
(135, '2018-11-30'),
(136, '2018-11-29'),
(137, '2018-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `repass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `name`, `email`, `phone`, `pass`, `repass`) VALUES
(36, 'tom', 'tom@gmail.com', '2147483647', '123', '123'),
(46, 'raj', 'raj@gmail.com', '8787878787', '123', '123'),
(47, 'vi', 'vi@gmail.com', '9234123455', '123', '123'),
(48, 'lakshmi', 'laxmi271997@gmail.com', '9632698763', 'lallu', 'lallu'),
(49, 'u@gmail.com', 'rr@gmail.com', '9123455677', '123', '123'),
(50, 'see', 'a@gmail.com', '9234567891', '1234', '1234'),
(51, 'Madhushree', 'madhu@gmail.com', '9740370101', '9740370101', '9740370101'),
(52, 'abi', 'abi@gmail.com', '9123456789', '12345678', '12345678'),
(53, 'ryi', 'tt@gmail.com', '9234567888', '123', '123'),
(54, 'hhhd', 't@gmail.com', '9123456777', '123', '123'),
(55, 'Aas', 's@gmIL.COM', '9123456789', 'sds', 'wer'),
(56, 'rerwew', 'd@gmail.com', '9123456788', '1Ab23456', '1Ab23456'),
(57, 'uyu', 'hkhhk@gmail.com', '9123456788', '1Ab12345', '1Ab12345');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `pnrno` int(10) NOT NULL,
  `ticket_no` int(10) NOT NULL,
  `source` varchar(10) NOT NULL,
  `train_dest` varchar(20) NOT NULL,
  `journey_date` date NOT NULL,
  `train_no` int(10) NOT NULL,
  `train_name` varchar(20) NOT NULL,
  `ticket_price` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `no_tickets` int(10) NOT NULL,
  `total_price` int(10) NOT NULL,
  `name_card` varchar(20) NOT NULL,
  `number_card` varchar(30) NOT NULL,
  `cvv` int(3) NOT NULL,
  `exp_date` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`pnrno`, `ticket_no`, `source`, `train_dest`, `journey_date`, `train_no`, `train_name`, `ticket_price`, `name`, `phone`, `no_tickets`, `total_price`, `name_card`, `number_card`, `cvv`, `exp_date`) VALUES
(20209, 201820, 'Bangalore', 'Ernakulum', '2018-11-10', 12284, 'duronto', 150, 'raj', '2147483647', 3, 450, 'raj', '1212121212', 123, 2035),
(20211, 262, 'Bangalore', 'Kolkata', '2018-11-05', 12859, 'geetanjali', 450, 'g', '0', 1, 450, 'gg', 'g', 0, 0),
(20213, 4133, 'Bangalore', 'Kolkata', '2018-11-02', 12859, 'geetanjali', 450, 'hh', '0', 1, 450, 'hh', 'hh', 0, 0),
(20214, 4115, 'Bangalore', 'Ernakulum', '2018-11-23', 12284, 'duronto', 150, 'vik', '2147483647', 2, 300, 'vik', '3729187368918', 200, 2020),
(20215, 4116, 'Bangalore', 'Kolkata', '2018-11-26', 12859, 'geetanjali', 450, 'dhf', '2147483647', 2, 900, 'xdfx', '564547756', 456, 2020),
(20216, 4119, 'Bangalore', 'Kolkata', '2018-11-23', 12859, 'geetanjali', 450, 'madhu', '2147483647', 2, 900, 'madhu', '234578945', 569, 2020),
(20217, 4108, 'Bangalore', 'Kolkata', '2018-11-28', 12859, 'geetanjali', 450, 'ryt', '91234', 3, 1350, 'eee', '3344555666', 345, 2020),
(20218, 4038, 'Bangalore', 'Kolkata', '2018-11-18', 12859, 'geetanjali', 450, 'tom', '9123456789', 1, 450, 'tom', '234566666666', 234, 2020),
(20219, 4135, 'Bangalore', 'Ernakulum', '2018-11-23', 12284, 'duronto', 150, 'abi', '9123456789', 1, 150, 'abi', '579828926912', 0, 0),
(20220, 4124, 'Bangalore', 'Ernakulum', '2018-11-23', 12284, 'duronto', 150, '', '', 1, 150, 'abi', '737268276236', 765, 2020),
(20221, 4125, 'Bangalore', 'Delhi', '2018-11-30', 12951, 'rajdhani', 600, 'abi', '9123456789', 1, 600, 'abi bb', '288764872336', 345, 2022),
(20222, 4077, 'Bangalore', 'Ernakulum', '2018-11-25', 12284, 'duronto', 150, 'raj', '8888888888', 5, 750, 'raj', '121212121299', 156, 2058);

-- --------------------------------------------------------

--
-- Table structure for table `trains`
--

CREATE TABLE `trains` (
  `t_no` int(10) NOT NULL,
  `t_name` varchar(30) DEFAULT NULL,
  `t_source` varchar(30) DEFAULT NULL,
  `t_destination` varchar(30) DEFAULT NULL,
  `no_of_seats` int(11) DEFAULT NULL,
  `ticket_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trains`
--

INSERT INTO `trains` (`t_no`, `t_name`, `t_source`, `t_destination`, `no_of_seats`, `ticket_price`) VALUES
(12859, 'geetanjali', 'Bangalore', 'Kolkata', 500, 450),
(12951, 'rajdhani', 'Bangalore', 'Delhi', 700, 600),
(85974, 'Madras Mail', 'Bangalore', 'Chennai', 550, 350);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adm`
--
ALTER TABLE `adm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care`
--
ALTER TABLE `care`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journey`
--
ALTER TABLE `journey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`pnrno`);

--
-- Indexes for table `trains`
--
ALTER TABLE `trains`
  ADD PRIMARY KEY (`t_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adm`
--
ALTER TABLE `adm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `care`
--
ALTER TABLE `care`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `journey`
--
ALTER TABLE `journey`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `pnrno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20223;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
