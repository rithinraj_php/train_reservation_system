<?php 
session_start();
include 'database.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  
}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("bg.JPG");
  
  /* Add the blur effect */
  filter: (8px);
  -webkit-filter: (8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 50%;
  padding: 50px;
  text-align: center;
}
@media print {
  #tick,#content,#logout,#print,#marquee {
    display: none;
  }
}


</style>
<title>Train reservation</title>

</head>
	
<body>

<div class="bg-image"></div>
<div class="bg-text">


<div>
  <?php
 
  if(!empty($_SESSION['SID']))
  {
    echo '<div id=""> ';
    echo '<tr>';
    echo "<td><span style='color:green;'></span> ".$_SESSION['SID']."</td>";
    echo  "<td ><a id='logout' href='logout.php' style='text-decoration: none; color:blue'> Logout</a></td>";
    echo '</tr>';
    echo '</div>';
  }
  else
  {
    echo '<div id=" "> ';
    echo '</div>';
  }
  ?>  
 </div>


<h2 id="marquee"><marquee>RAILWAY RESERVATION SYSTEM</marquee></h2>
<div>
            <?php 
				if($_GET){
					if($_GET['msg']=="User Register Successfully")
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:green;">'.$_GET['msg'].'</label>';
						echo '<br>';
						echo"<a style='font-weight: bold; text-align:center; font-size:10pt; text-decoration: none;' href='index.php'>Please Login</a>";
					}
					else
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:red;">'.$_GET['msg'].'</label>';
			
					}	
				}				   
			?>
</div>
				
<table align="center">
        <h1>Train Ticket</h1>
  <tr>
    <td align="center">        
   <form action="print.php" method="POST">
        <div>
                  <td id="myid1">
                    <?php
                      if(isset($_POST['print']))
                      {
                    
                      $sql = "SELECT * FROM ticket ORDER BY pnrno DESC LIMIT 1";
                      $rs = mysqli_query($con,$sql);                   

                      echo "<table style='background-color:white;border-color:white;border-radius:10px' border=1 align=center>";

                      while($row=mysqli_fetch_row($rs))
                      {
                      echo "<tr><td style='color:red;font-weight:bold'>PNRNO <td style='color:black;font-weight:100'>$row[0]<td style='color:red;font-weight:bold'>Ticket Number <td style='color:black;font-weight:100'>$row[1] 

                      <tr><td style='color:red;font-weight:bold'>Source <td style='color:black;font-weight:100'>$row[2]<td style='color:red;font-weight:bold'>Destination <td style='color:black;font-weight:100'>$row[3]
                      
                      <tr><td style='color:red;font-weight:bold'>Train Name <td style='color:black;font-weight:100'>$row[6]<td style='color:red;font-weight:bold'>Train Number <td style='color:black;font-weight:100'>$row[5]
                      
                      <tr><td style='color:red;font-weight:bold'>Passenger Name <td style='color:black;font-weight:100'>$row[8]<td style='color:red;font-weight:bold'>Phone Number <td style='color:black;font-weight:100'>$row[9]
                      
                      <tr><td style='color:red;font-weight:bold'>Ticket Price <td style='color:black;font-weight:100'>$row[7]<td style='color:red;font-weight:bold'>Number of Tickets <td style='color:black;font-weight:100'>$row[10]
                      
                      <tr><td style='color:red;font-weight:bold'>Total Price <td style='color:black;font-weight:100'>$row[11]<td style='color:red;font-weight:bold'>Date of Journey <td style='color:black;font-weight:100'>$row[4]";
                      }
                      echo "</table>";
                      echo "<br/>";
                      echo "<input id='tick' type='button' onclick='window.print();' value='Print Ticket'>";
                      }
                    ?>

                  </td>

        </div>
              <div>  
                      <input id="print" type="submit" onclick="ticket()" name="print" value="View Ticket"/>
              
              </div>

</form>
    </td>
    

    <td align="right" id="content">
     <h3>1. Search</h3>
     <h3>2. Train details</h3>
     <h3>3. Passenger details</h3>
     <h3>4. Payment</h3>
     <h3 style="color: red;">5. Print ticket</h3>
    </td>  

  </tr>
</table>

</body>

<script>
  
function ticket(){
    if(document.getElementById('tick').style.display == 'none'){
    document.getElementById('tick').style.display = 'block';
    }

  }


</script>

</html>