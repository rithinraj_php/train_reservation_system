<?php 
session_start();
include 'database.php';
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  
}

* {
  box-sizing: border-box;
}
.bg-image {
  /* The image used */
  background-image: url("../bg.JPG");
  
  /* Add the blur effect */
  filter: (8px);
  -webkit-filter: (8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 100%;
  padding: 100px;
  text-align: center;
}
</style>

<title>Train reservation</title>

</head>
  
<body>
<div class="bg-image"></div>
<div class="bg-text">

<div>
  <?php
 
  if(!empty($_SESSION['SID']))
  {
    echo '<div id=""> ';
    echo '<tr>';
    echo "<td><span style='color:green;'>Welcome:</span> ".$_SESSION['SID']."</td>";
    echo "<td><span style='color:green;'><a href='adhome.php'>Logout</a></td>";
    echo "<td><a href='adhome.php'><input type='button' value='back'></a></td>";
    echo '</tr>';
    echo '</div>';
  }
  else
  {
    echo '<div id=" "> ';
    echo '</div>';
  }
  ?>  
 </div>



<table> 
<tr>

<!--add-train-->
<td align="left">
        <div> 
            <form method="POST" action="trial.php" enctype="multipart/form-data">
              <h2><marquee>RAILWAY RESERVATION SYSTEM</marquee></h2>

            <div>
              <?php 
                if($_GET){
                if($_GET['msg']=="Added Successfully")
                {
                echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:green;">'.$_GET['msg'].'</label>';
                }
                else
                {
                
                } 
                }          
              ?>
            </div>

              <h1>Add New Train</h1>
              <table>

              <tr>
                <td align="left">Train no:</td>
                <td> <input type="int" name="t_no" title="Only Numbers" required></td>
              </tr>
              <tr>
                    <td align="left">Train Name:</td> 
                      <td><input type="text" name="t_name" maxlength="20"  title="Only Alphabets" required></td>
                    </tr>
                    <tr>
                      <td align="left">Train Source:</td> 
                      <td><input type="text" name="t_source" value="Bangalore" readonly  required>

                  </td>
                    </tr>
                    <tr>
                   <td align="left">Train Destination:</td> 
                      <td><input type="text" maxlength="20" name="t_destination" value=""  required>
                  </td>
                </tr>
                <tr>
               <td align="left">TNo of seats: </td>
                      <td><input type="int" name="no_of_seats" value=""  required>
                  </td>
                </tr>
                <tr>
                  <td align="left">Ticket Price: </td>
                      <td><input type="int" name="ticket_price" value=""  required>
                  </td>
              </tr>
              </table>
              <table align="center">
                <tr>
                  <td>
                <input type="submit" name="add" value="submit">
              </tr>
            </td>
              </table>

            </form>         
        </div>
</td>
<!-- add-tarin -->


<!-- view -->
<td align="center">
<form method="POST" action="traindetails.php" enctype="multipart/form-data">
    <table>
      <tr> 
      <h2>View Trains</h2>

        <td>
          <?php
            if(isset($_POST['view']))
            {
            
            // extract($_SESSION);
            $sql = "SELECT * FROM trains";
            $rs = mysqli_query($con,$sql);

            // echo "<h1 class=head1>View Trains</h1>";
            // if(mysqli_num_rows($rs)<1)
            // {
            // echo "<br><br><h1 class=head1> Train detils not found</h1>";
            // exit;
            // }
            echo "<table border=1 align=center><tr style='color:red'><td>Train No <td> Train Name<td> Destination <td> Ticket Price";
            while($row=mysqli_fetch_row($rs))
            {
            echo "<tr><td>$row[0] <td align=center> $row[1] <td align=center> $row[3]<td align=center> $row[5]";
            }
            echo "</table>";
            }
          ?>
        </td>
      </tr>

      <tr>
          <td><input type="submit" name="view" value="View Train Details"></td>
      </tr>

    </table>
</form>
</td>
<!-- view -->


<!-- delete -->
<td align="right">
<form method="POST" action="traindetails.php">

    <div>
          <?php 
            if($_GET){
            if($_GET['msg']=="deleted Successfully")
            {
            echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:green;">'.$_GET['msg'].'</label>';
            }
            else
            {


            } 
            }          
          ?>
    </div>

    <table align="center">
      <tr align="center">
        <h2>Delete Train</h2>
        <td><input type="text" name="t_no" placeholder="train number"></td>
        <td><input type="submit" name="delete"></td>
      </tr>
    </table>

    <?php
    // include 'database.php';
    if(isset($_POST['delete']))
    {
        $t_no = $_POST['t_no'];

        $msg2="deleted Successfully";
        $msg="not Success";
      
        $sql="delete from trains where t_no = '$t_no'";
           $result =  mysqli_query($con,$sql);
                header("Location:traindetails.php?msg=".$msg2);
      }
    ?>
</form>
</td>
<!-- delete -->

</body>
</html>