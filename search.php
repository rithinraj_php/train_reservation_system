<?php 
session_start();
include 'database.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  
}

* {
  box-sizing: border-box;
}
.bg-image {
  /* The image used */
  background-image: url("bg.JPG");
  
  /* Add the blur effect */
  filter: (8px);
  -webkit-filter: (8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 50%;
  padding: 50px;
  text-align: center;
}
</style>

<title>Train reservation</title>

</head>
	
<body>
<div class="bg-image"></div>
<div class="bg-text">

<div>
  <?php
 
  if(!empty($_SESSION['SID']))
  {
    echo '<div id=""> ';
    echo '<tr>';
    echo "<td><span style='color:green;'>Welcome:</span> ".$_SESSION['SID']."</td>";

    echo '</tr>';
    echo '</div>';
  }
  else
  {
    echo '<div id=" "> ';
    echo '</div>';
  }
  ?>  
 </div>

		
<table align="center">    
  <tr> 
    <td align="center">   
        <div> 
          	<form method="POST" action="search_result.php" enctype="multipart/form-data">
          		<h2><marquee>RAILWAY RESERVATION SYSTEM</marquee></h2>
          		<h1>Search Train</h1>
              <table align="center">

              <tr>

                <td>From: <input type="text" name="from" value="Bangalore" disabled></td>
                    <td>To: 
                      <?php
                      $sql = "SELECT t_destination FROM trains";
                      $result = mysqli_query($con,$sql);

                      echo '<select id="class" name="t_destination">';
                      echo "<option value='0'>- Select -</option>";

                      while ($row = mysqli_fetch_assoc($result)) {
                      echo "<option value={$row['t_destination']}>{$row['t_destination']}</option>";
                      }

                      echo '</select>';
                      ?>
                  </td>
                <td>Date Of journey: <input type="date" name="journey_date"></td>  
              </tr>
              </table>    
              <input type="submit" name="submit" value="search">
              <a href="home.php"><input id="back" type="button" value="Back"/></a>
          	</form>					
        </div>
    </td>

    <td align="right">
     <h3 style="color: red;">1. Search</h3>
     <h3 >2. Train details</h3>
     <h3>3. Passenger details</h3>
     <h3>4. Payment</h3>
     <h3>5. Print ticket</h3>
    </td>    

  </tr>
</table>

</body>
</html>